/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.openvas;

import de.cbless.vulnscan.api.ClientException;
import de.cbless.vulnscan.api.SessionException;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.Vulnerability;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Christoph Bless
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OpenVASReportTest {
    
    
    private OpenVASClient client;
    
    public OpenVASReportTest() {
        try {
            client = new OpenVASClient(TestConfiguration.HOST, TestConfiguration.PORT, true);
            client.login(TestConfiguration.USERNAME, TestConfiguration.PASSWORD);
        } catch (SessionException ex) {
            fail(ex.getMessage());
        } catch (ClientException ex) {
            fail(ex.getMessage());
        } 
    }
    
    private String getScanId() throws ClientException{
        List<Scan> scans = client.getScans();
        if (scans.size() > 0 ){
            return scans.get(0).getId();
        }
        throw new IllegalStateException("Test requires at least one Task in "
                + "your OpenVAS installation.");
    }
    
    
    /**
     * Test of getResults method, of class OpenVASClient.
     */
    @Test
    public void testGetResults(){
        System.out.println("getResults");
        try{
            client.login(TestConfiguration.USERNAME, TestConfiguration.PASSWORD);
            String id = getScanId();
            List<Vulnerability> vulns = client.getResults(id);
            for (Vulnerability v : vulns){
                assertNotNull(v.getId());
            }
        } catch (Exception ex){
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void testGetResultsByHost() {
        System.out.println("getResultsByHost");
        try{
            client.login(TestConfiguration.USERNAME, TestConfiguration.PASSWORD);
            String id = getScanId();
            Map<String, List<Vulnerability>> vulns = client.getResultsByHost(id);
            
            for (String k : vulns.keySet()){
                assertFalse(vulns.get(k).isEmpty());
            }
        } catch (Exception ex){
            fail(ex.getMessage());
        }
    }
}

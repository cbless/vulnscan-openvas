/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.openvas;

import de.cbless.vulnscan.api.ClientException;
import de.cbless.vulnscan.api.SessionException;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.ScanDetails;
import de.cbless.vulnscan.api.model.Vulnerability;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Christoph Bless
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OpenVASClientTest {
    
    
    private OpenVASClient client;
    
    public OpenVASClientTest() {
        try {
            client = new OpenVASClient(TestConfiguration.HOST, 
                    TestConfiguration.PORT, true);
        } catch (ClientException ex) {
            fail(ex.getMessage());
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isAuthenticated method, of class OpenVASClient.
     */
    @Test
    public void test01IsAuthenticated() {
        System.out.println("isAuthenticated");
        boolean ret = client.isAuthenticated();
        assertFalse(ret);
    }
    
    /**
     * Test of login method, of class OpenVASClient.
     */
    @Test
    public void test02Login() {
        System.out.println("login");
        
        try {
            client.login(TestConfiguration.USERNAME, 
                    TestConfiguration.PASSWORD);
        } catch (SessionException ex) {
            fail(ex.getMessage());
        } catch (ClientException ex) {
            fail(ex.getMessage());
        }
        
    }

    /**
     * Test of isAuthenticated method, of class OpenVASClient.
     */
    @Test
    public void test03IsAuthenticated() {
        System.out.println("isAuthenticated");
        try {
            client.login(TestConfiguration.USERNAME, 
                    TestConfiguration.PASSWORD);
            boolean ret = client.isAuthenticated();
            assertTrue(ret);
        } catch (SessionException ex) {
            fail(ex.getMessage());
        } catch (ClientException ex) {
            fail(ex.getMessage());
        }
    }
    
    /**
     * Test of logout method, of class OpenVASClient.
     */
    @Test
    public void test04Logout(){
        System.out.println("logout");
        try {
            client.logout();
        } catch (SessionException ex) {
            fail(ex.getMessage());
        } catch (ClientException ex) {
            fail(ex.getMessage());
        }
    }

    

    /**
     * Test of getScans method, of class OpenVASClient.
     */
    @Test
    public void test05GetScans() throws Exception {
        System.out.println("getScans");
        client.login(TestConfiguration.USERNAME, 
                    TestConfiguration.PASSWORD);
        List<Scan> scans = client.getScans();
        for (Scan scan : scans){
            assertNotNull(scan.getId());
            assertNotNull(scan.getName());
            assertNotNull(scan.getStatus());
        }
    }

    private String getScanId() throws ClientException{
        List<Scan> scans = client.getScans();
        if (scans.size() > 0 ){
            return scans.get(0).getId();
        }
        throw new IllegalStateException("Test requires at least one Task in "
                + "your OpenVAS installation.");
    }
    
    /**
     * Test of getScan method, of class OpenVASClient.
     */
    @Test
    public void test06GetScan() {
        System.out.println("getScan");
        try{
            client.login(TestConfiguration.USERNAME, 
                    TestConfiguration.PASSWORD);
            String id = getScanId();
            Scan scan = client.getScan(id);
            assertThat(scan.getId(), equalTo(id));
        } catch(Exception ex){
            fail(ex.getMessage());
        } 
        
        
    }

    /**
     * Test of getScanDetails method, of class OpenVASClient.
     */
    @Test
    public void test07GetScanDetails_0args() {
        System.out.println("getScanDetails");
        try{
            client.login(TestConfiguration.USERNAME, 
                    TestConfiguration.PASSWORD);
            List<ScanDetails> scans = client.getScanDetails();
            for (ScanDetails s : scans){
                assertNotNull(s.getId());
                assertNotNull(s.getName());
                assertNotNull(s.getStatus());
                assertNotNull(s.getTargets());
            }
        } catch (Exception ex){
            fail(ex.getMessage());
        }
    }

    /**
     * Test of getScanDetails method, of class OpenVASClient.
     */
    @Test
    public void test08GetScanDetails_String() throws Exception {
        System.out.println("getScanDetails");
        try{
            client.login(TestConfiguration.USERNAME, 
                    TestConfiguration.PASSWORD);
            String id = getScanId();
            ScanDetails scan = client.getScanDetails(id);
            assertThat(scan.getId(), equalTo(id));
        } catch (Exception ex){
            fail(ex.getMessage());
        }
    }

//    /**
//     * Test of launch method, of class OpenVASClient.
//     */
//    @Test
//    public void test09Launch() throws Exception {
//        System.out.println("launch");
//        try{
//            client.login(USERNAME, PASSWORD);
//            String id = getScanId();
//            String ret = client.launch(id);
//            System.out.println("Launch ret: " +ret);
//            assertNotNull(ret);
//            Thread.sleep(5000);
//        } catch (Exception ex){
//            fail(ex.getMessage());
//        }
//    }
//
//    /**
//     * Test of pause method, of class OpenVASClient.
//     */
//    @Test
//    public void test10Pause() throws Exception {
//        System.out.println("pause");
//        try{
//            client.login(USERNAME, PASSWORD);
//            String id = getScanId();
//            client.pause(id);
//            Thread.sleep(5000);
//        } catch (Exception ex){
//            fail(ex.getMessage());
//        }
//    }
//
//    /**
//     * Test of resume method, of class OpenVASClient.
//     */
//    @Test
//    public void test11Resume() throws Exception {
//        System.out.println("resume");
//        try{
//            client.login(USERNAME, PASSWORD);
//            String id = getScanId();
//            boolean running = true;
//            while (running){
//                String status = client.getScanStatus(id);
//                if (TaskStatus.StopRequested.getStatus().equals(status)){
//                    System.out.println("waiting until task reached the required"
//                            + " state 'stopped'. Current status is '"+ status 
//                            + "'.");
//                    Thread.sleep(5000);
//                } else if (TaskStatus.Stopped.getStatus().equals(status)){
//                    break;
//                } else {
//                    fail("Can not resume task, while it is in status '" + status + "'");
//                }
//            }
//            String ret = client.resume(id);
//            assertNotNull(ret);
//        } catch (Exception ex){
//            fail(ex.getMessage());
//        }
//    }
//
//    /**
//     * Test of stop method, of class OpenVASClient.
//     */
//    @Test
//    public void test12Stop() throws Exception {
//        System.out.println("stop");
//        try{
//            client.login(USERNAME, PASSWORD);
//            String id = getScanId();
//            client.pause(id);
//        } catch (Exception ex){
//            fail(ex.getMessage());
//        }
//    }

    /**
     * Test of getResults method, of class OpenVASClient.
     */
    @Test
    public void testGetResults() throws Exception {
        System.out.println("getResults");
        try{
            client.login(TestConfiguration.USERNAME, 
                    TestConfiguration.PASSWORD);
            String id = getScanId();
            List<Vulnerability> vulns = client.getResults(id);
            for (Vulnerability v : vulns){
                assertNotNull(v.getId());
                //System.out.println(v);
            }
        } catch (Exception ex){
            fail(ex.getMessage());
        }
    }
    
}

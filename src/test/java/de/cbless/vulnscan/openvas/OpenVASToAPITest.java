/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.openvas;

import de.cbless.openvasclient.model.resources.CertRef;
import de.cbless.openvasclient.model.resources.NVT;
import de.cbless.openvasclient.model.resources.results.Result;
import de.cbless.openvasclient.model.resources.tasks.Target;
import de.cbless.openvasclient.model.resources.tasks.Task;
import de.cbless.vulnscan.api.model.Reference;
import de.cbless.vulnscan.api.model.ReferenceType;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.ScanDetails;
import de.cbless.vulnscan.api.model.Vulnerability;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Bless
 */
public class OpenVASToAPITest {
    
    private final Task task;
    private final Target target;
    private final Result result;
    private final NVT nvt;
    
    public OpenVASToAPITest() {
        target = new Target();
        target.setId("123");
        target.setName("Target");
        
        task = new Task();
        task.setId("1234567");
        task.setName("Taskname");
        task.setStatus("Running");
        task.setTarget(target);result = new Result();
        
        nvt = new NVT();
        nvt.setOid("12344");
        nvt.setName("NVT Name");
        nvt.setFamily("NVT Family");
        nvt.setBid("1234");
        nvt.setCve("1234");
        nvt.setCvssBase("5.0");
        nvt.setTags("Tag1,Tag2");
        nvt.setBugtraqId("12344555");
        List<CertRef> refs = new ArrayList<CertRef>();
        CertRef ref = new CertRef();
        ref.setId("1233444");
        ref.setType(ReferenceType.CERT.toString());
        refs.add(ref);
        nvt.setCertRefs(refs);
        
        result.setId("123");
        result.setName("ResultName");
        result.setComment("Description");
        result.setCreationTime("123333");
        result.setModificationTime("123333");
        result.setHost("127.0.0.1");
        result.setPort("445");
        result.setTask(task);
        result.setNvt(nvt);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toScan method, of class OpenVASToAPI.
     */
    @Test
    public void testToScan() {
        Scan scan = OpenVASToAPI.toScan(task);
        assertThat(scan.getId(), equalTo(task.getId()));
        assertThat(scan.getName(), equalTo(task.getName()));
        assertThat(scan.getStatus(), equalTo(task.getStatus()));
    }

    /**
     * Test of toScanDetails method, of class OpenVASToAPI.
     */
    @Test
    public void testToScanDetails() {
        ScanDetails scan = OpenVASToAPI.toScanDetails(task);
        assertThat(scan.getId(), equalTo(task.getId()));
        assertThat(scan.getName(), equalTo(task.getName()));
        assertThat(scan.getStatus(), equalTo(task.getStatus()));
        assertThat(scan.getTargets(), equalTo(task.getTarget().getName()));
    }

    /**
     * Test of refFromBID method, of class OpenVASToAPI.
     */
    @Test
    public void testRefFromBID() {
        String bid = "12344555";
        Reference result = OpenVASToAPI.refFromBID(bid);
        assertEquals(bid, result.getReference());
    }

    /**
     * Test of refFromCVE method, of class OpenVASToAPI.
     */
    @Test
    public void testRefFromCVE() {
        String cve = "2015-00001";
        Reference result = OpenVASToAPI.refFromCVE(cve);
        assertEquals(cve, result.getReference());
    }

    /**
     * Test of toVuln method, of class OpenVASToAPI.
     */
    @Test
    public void testToVuln() {
        Vulnerability vuln = OpenVASToAPI.toVuln(result);
        assertThat(result.getId(), equalTo(vuln.getId()));
        assertThat(result.getName(), equalTo(vuln.getName()));
        assertThat(result.getSeverity(), equalTo(vuln.getSeverity()));
    }
    
}

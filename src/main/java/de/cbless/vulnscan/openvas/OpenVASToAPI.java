/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.openvas;

import de.cbless.openvasclient.model.resources.CertRef;
import de.cbless.openvasclient.model.resources.NVT;
import de.cbless.openvasclient.model.resources.NvtFamily;
import de.cbless.openvasclient.model.resources.Target;
import de.cbless.openvasclient.model.resources.results.Result;
import de.cbless.openvasclient.model.resources.tasks.Task;
import de.cbless.vulnscan.api.model.Plugin;
import de.cbless.vulnscan.api.model.PluginFamily;
import de.cbless.vulnscan.api.model.Reference;
import de.cbless.vulnscan.api.model.ReferenceType;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.ScanDetails;
import de.cbless.vulnscan.api.model.Vulnerability;
import java.util.ArrayList;
import java.util.List;

/**
 * This class converts the types form the OpneVASClient to types of the 
 * VulnScan-API.
 * 
 * @author Christoph Bless
 */
public class OpenVASToAPI {
    
    public static final int NONE_EXISTING_ID = -1;
    
    /**
     * Converts an instance of the Task class (OpenVASClient) to an instance 
     * of the Scan class (VulnScan-API).
     * 
     * @param task Instance of the Task class (OpenVASClient) 
     * @return Instance of the Scan class (VulnScan-API)
     */
    public static Scan toScan(Task task){
        Scan s = new Scan();
        s.setId(task.getId());
        s.setName(task.getName());
        s.setStatus(task.getStatus());
        return s;
    }
    
    /**
     * Converts an instance of the Task class (OpenVASClient) to an instance 
     * of the ScanDetails class (VulnScan-API).
     * 
     * @param task Instance of the Task class (OpenVASClient) 
     * @return Instance of the ScanDetails class (VulnScan-API)
     */
    public static ScanDetails toScanDetails(Task task){
        ScanDetails s = new ScanDetails();
        s.setId(task.getId());
        s.setName(task.getName());
        s.setStatus(task.getStatus());
        s.setTargets(task.getTarget().getName());
        return s;
    }
    
    /**
     * Converts an instance of the Task class and the Target class 
     * (OpenVASClient) to an instance of the Scan class (VulnScan-API).
     * 
     * @param task Instance of the Task class (OpenVASClient) 
     * @param target Instance of the Target class (OpenVASClient) 
     * @return Instance of the Scan class (VulnScan-API)
     */
    public static ScanDetails toScanDetails(Task task, Target target){
        ScanDetails s = new ScanDetails();
        s.setId(task.getId());
        s.setName(task.getName());
        s.setStatus(task.getStatus());
        s.setTargets(target.getHosts());
        return s;
    }
    
    /**
     * Creates a Reference with type BID from the given String.
     * 
     * @param bid Reference as String
     * @return Reference
     */
    public static Reference refFromBID(String bid){
        Reference ref = new Reference();
        ref.setType(ReferenceType.BID);
        ref.setReference(bid);
        return ref;
    }
    
    /**
     * Creates a Reference with type BUGTRAQ from the given String.
     * 
     * @param bid Reference as String
     * @return Reference
     */
    public static Reference refFromBuqtraq(String bid){
        Reference ref = new Reference();
        ref.setType(ReferenceType.BUGTRAQ);
        ref.setReference(bid);
        return ref;
    }
    
    /**
     * Creates a Reference with type CVE from the given String.
     * 
     * @param cve Reference as String
     * @return Reference
     */
    public static Reference refFromCVE(String cve){
        Reference ref = new Reference();
        ref.setType(ReferenceType.CVE);
        ref.setReference(cve);
        return ref;
    }
    
    /**
     * Creates a Reference with type XREF from the given String.
     * 
     * @param xref Reference as String
     * @return Reference
     */
    public static Reference refFromXREF(String xref){
        Reference ref = new Reference();
        ref.setType(ReferenceType.XREF);
        ref.setReference(xref);
        return ref;
    }
    
    /**
     * Creates a Reference with type CERT from the given String.
     * 
     * @param certRef  Reference as String
     * @return Reference
     */
    public static Reference refFromCert(CertRef certRef){
        Reference ref = new Reference();
        ref.setType(ReferenceType.CERT);
        ref.setReference(certRef.toString());
        return ref;
    }
    
    /**
     * Converts an instance of the Result class (OpenVASClient) to an instance 
     * of the Vulnerability class (VulnScan-API).
     * 
     * @param result Instance of the Result class (OpenVASClient) 
     * @return Instance of the Vulnerability class (VulnScan-API)
     */
    public static Vulnerability toVuln(Result result){
        Vulnerability vuln = new Vulnerability();
        vuln.setId(result.getId());
        vuln.setName(result.getName());
        vuln.setSeverity(result.getSeverity());
        vuln.setPluginId(result.getNvt().getOid());
        
        return vuln;
    }
    
    /**
     * Converts an instance of the NVT class (OpenVASClient) to a list of 
     * References (VulnScan-API).
     * 
     * @param nvt Instance of the NVT class (OpenVASClient) 
     * @return list of References (VulnScan-API)
     */
    public static List<Reference> toReferences(NVT nvt){
        List<Reference> refs = new ArrayList<Reference>();
        String cve = nvt.getCve();
        String bugtraq = nvt.getBugtraqId();
        String xref = nvt.getXref();
        String bid = nvt.getBid();
        if (bid != null && !"NOBID".equals(bid)){
            refs.add(refFromBID(bid));
        }
        if (cve != null && !"NOCVE".equals(cve)){
            refs.add(refFromCVE(cve));
        }
        if (bugtraq != null){
            refs.add(refFromBuqtraq(bugtraq));
        }
        if (xref != null && !"NOXREF".equals(xref)){
            refs.add(refFromXREF(xref));
        }
        for (CertRef ref : nvt.getCertRefs()){
            refs.add(refFromCert(ref));
            
        }
        return refs;
    }
    
    /**
     * Converts an instance of the NVT class (OpenVASClient) to an instance 
     * of the Plugin class (VulnScan-API).
     * 
     * @param nvt Instance of the NVT class (OpenVASClient) 
     * @return Instance of the Plugin class (VulnScan-API)
     */
    public static Plugin toPlugin(NVT nvt){
        System.out.println(nvt);
        Plugin plugin = new Plugin();
        plugin.setId(nvt.getOid());
        plugin.setName(nvt.getName());
        plugin.setFamily(nvt.getFamily());
        plugin.setCvss(nvt.getCvssBase());
        plugin.setReferences(toReferences(nvt));
        plugin.setDescription(nvt.getSummary());
        return plugin;
    }
    
    
    /**
     * Converts an instance of the NvtFamily class (OpenVASClient) to an 
     * instance of the PluginFamily class (VulnScan-API).
     * 
     * @param f Instance of the NvtFamily class (OpenVASClient) 
     * @return Instance of the PluginFamily class (VulnScan-API)
     */
    public static PluginFamily toPluginFamily(NvtFamily f){
        PluginFamily family = new PluginFamily();
        family.setId(String.valueOf(NONE_EXISTING_ID)); // nvt Family has no id
        family.setName(f.getName());
        family.setPluginCount(f.getNvtCount());
        return family;
    }
}

/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.vulnscan.openvas;

import de.cbless.openvasclient.ClientBuilder;
import de.cbless.openvasclient.ConfigManager;
import de.cbless.openvasclient.LoginException;
import de.cbless.openvasclient.ManagerFactory;
import de.cbless.openvasclient.OMPClient;
import de.cbless.openvasclient.OpenVASException;
import de.cbless.openvasclient.PluginManager;
import de.cbless.openvasclient.ScanManager;
import de.cbless.openvasclient.model.resources.NVT;
import de.cbless.openvasclient.model.resources.NvtFamily;
import de.cbless.openvasclient.model.resources.Target;
import de.cbless.openvasclient.model.resources.results.Result;
import de.cbless.openvasclient.model.resources.tasks.Task;
import de.cbless.openvasclient.model.responses.GetTasksResponse;
import de.cbless.openvasclient.model.responses.ResumeTaskResponse;
import de.cbless.openvasclient.model.responses.StartTaskResponse;
import de.cbless.vulnscan.api.AbstractClient;
import de.cbless.vulnscan.api.ClientException;
import de.cbless.vulnscan.api.NotFoundException;
import de.cbless.vulnscan.api.SessionException;
import de.cbless.vulnscan.api.model.Plugin;
import de.cbless.vulnscan.api.model.PluginFamily;
import de.cbless.vulnscan.api.model.Scan;
import de.cbless.vulnscan.api.model.ScanDetails;
import de.cbless.vulnscan.api.model.Vulnerability;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class implements all interfaces from the VulnScan-API. It provides 
 * access the Scans/Tasks, to plugin information or to the result of the tasks.
 * 
 * @author Christoph Bless
 */
public class OpenVASClient extends AbstractClient{

    private OMPClient client;
    private ScanManager scanManager;
    private PluginManager pluginManager;
    private ConfigManager configManager;
    
    public OpenVASClient(String host, int port, boolean trustAll) throws ClientException {
        super(host, port, trustAll);
        ClientBuilder builder = new ClientBuilder(host, port, trustAll);
        try {
            client = builder.build();
        } catch (IOException ex) {
            throw new ClientException(ex);
        } catch (KeyManagementException ex) {
            throw new ClientException(ex);
        } catch (NoSuchAlgorithmException ex) {
            throw new ClientException(ex);
        }
        ManagerFactory factory = new ManagerFactory(client);
        scanManager = factory.createScanManager();
        pluginManager = factory.createPluginManager();
        configManager = factory.createConfigManager();
    }

    
    @Override
    public void login(String username, String password) throws SessionException, ClientException {
        client.setUsername(username);
        client.setPassword(password);
        try {
            client.authenticate();
        } catch(LoginException ex ) {
            throw new SessionException(ex);
        }catch (OpenVASException ex) {
            throw new ClientException(ex);
        }
    }

    @Override
    public void logout() throws SessionException, ClientException {
        // Since OpenVAS doesn't support a Logout this method won't do anything.
    }

    @Override
    public boolean isAuthenticated() {
        return client.isAuthenticated();
    }

    @Override
    public List<Scan> getScans() throws ClientException {
        List<Scan> results = new ArrayList<Scan>();
        try {
            GetTasksResponse response = scanManager.getTasks();
            for (Task t : response.getTasks()){
                results.add(OpenVASToAPI.toScan(t));
            }
            return results;
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }        
    }

    @Override
    public Scan getScan(String id) throws NotFoundException, ClientException {
        try {
            GetTasksResponse response = scanManager.getTasks();
            if (response.getTasks().isEmpty()){
                throw new NotFoundException("Scan with id: " + id + " not found!");
            }
            return OpenVASToAPI.toScan(response.getTasks().get(0));
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }  
    }

    public List<ScanDetails> getScanDetails() throws ClientException {
        List<ScanDetails> results = new ArrayList<ScanDetails>();
        try {
            GetTasksResponse response = scanManager.getTasks();
            for (Task t : response.getTasks()){
                List<Target> targets = configManager.getTargets(t.getTarget().getId());
                if (targets.isEmpty()){
                    results.add(OpenVASToAPI.toScanDetails(t));
                } else {
                    results.add(OpenVASToAPI.toScanDetails(t, targets.get(0)));
                }
            }
            return results;
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }
    }

    @Override
    public ScanDetails getScanDetails(String id) throws ClientException, NotFoundException {
        try {
            GetTasksResponse response = scanManager.getTasks();
            if (response.getTasks().isEmpty()){
                throw new NotFoundException("Scan with id: " + id + " not found!");
            }
            return OpenVASToAPI.toScanDetails(response.getTasks().get(0));
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }  
    }

    @Override
    public String launch(String id) throws ClientException {
        try {
            StartTaskResponse response = scanManager.startTask(id);
            return response.getReportId();
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }
    }

    @Override
    public void pause(String id) throws ClientException {
        try {
            scanManager.stopTask(id);
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }
    }

    @Override
    public String resume(String id) throws ClientException {
        try{
            ResumeTaskResponse response = scanManager.resumeTask(id);
            return response.getReportId();
        } catch (OpenVASException ex){
            throw new ClientException(ex);
        }
    }

    @Override
    public void stop(String id) throws ClientException {
        try {
            scanManager.stopTask(id);
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }
    }

    @Override
    public List<Vulnerability> getResults(String id) throws ClientException {
        try {
            List<Vulnerability> vulns = new ArrayList<Vulnerability>();
            List<Result> results = scanManager.getResults(id);
            for (Result r : results){
                
                vulns.add(OpenVASToAPI.toVuln(r));
            }
            return vulns;
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }
    }
    
    @Override
    public Map<String, List<Vulnerability>> getResultsByHost(String id) 
            throws ClientException {
        try {
            Map<String, List<Vulnerability>> results = new HashMap<String , List<Vulnerability>>();
            List<Result> response = scanManager.getResults(id);
            for (Result r : response){
                List<Vulnerability> vulns;
                if (results.containsKey(r.getHost())){
                    vulns = results.get(r.getHost());
                } else {
                    vulns = new ArrayList<Vulnerability>();
                    
                }
                vulns.add(OpenVASToAPI.toVuln(r));
                results.put(r.getHost(), vulns);
            }
            return results;
        } catch (OpenVASException ex){
            throw new ClientException(ex);
        }
    }

    @Override
    public String getScanStatus(String id) throws ClientException, NotFoundException {
        Scan scan = getScan(id);
        return scan.getStatus();
    }

    @Override
    public List<PluginFamily> listFamilies() throws ClientException {
        try{
            List<NvtFamily> nvtFamilies = pluginManager.getFamilies();
            List<PluginFamily> results = new ArrayList<PluginFamily>();
            for(NvtFamily f : nvtFamilies){
                results.add(OpenVASToAPI.toPluginFamily(f));
            }
            return results;
        } catch (OpenVASException ex){
            throw new ClientException(ex);
        }
    }

    @Override
    public Plugin getPlugin(String id) throws NotFoundException, ClientException {
        try {
            List<NVT> nvts = pluginManager.getNVT(id);
            if (nvts.size() > 0){
                return OpenVASToAPI.toPlugin(nvts.get(0));
            }
            throw new NotFoundException("plugin not found!");
        } catch (OpenVASException ex) {
            throw new ClientException(ex);
        }
    }

}
